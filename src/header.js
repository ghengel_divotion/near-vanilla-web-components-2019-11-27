import styles from './header.css.js';

class TodoMVCHeader extends HTMLElement {
  inputElement;

  connectedCallback() {
    this.innerHTML = `
      <header class="header">
        <h1>Todos</h1>
        <input class="new-todo2" placeholder="What needs to be done?" autofocus>
      </header>
      
      <style>${styles}</style>
    `;

    this.inputElement = this.querySelector('input');

    this.inputElement.addEventListener('keyup', this._handleKeyup.bind(this));
  }

  _handleKeyup(event) {
    if (event.key === 'Enter') {
      this._addTodo();
    }
  }

  _addTodo() {
    const newTodoText = this.inputElement.value.trim();

    if (newTodoText) {
      this.dispatchEvent(new CustomEvent('todomvc-item-added', {detail: newTodoText}));
      this.inputElement.value = '';
    }
  }
}


if (!customElements.get('todomvc-header')) {
  customElements.define('todomvc-header', TodoMVCHeader);
}
