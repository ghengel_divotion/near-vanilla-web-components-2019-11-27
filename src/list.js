import {store} from "./store.js";
import styles from "./list.css.js";

import './header.js';
import './item.js';

class TodoMVCList extends HTMLElement {
  _currentStatus = '';

  connectedCallback() {
    this._update();
  }

  get _todos() {
    if (this._currentStatus === 'completed') {
      return store.getCompleted();
    } else if (this._currentStatus === 'active') {
      return store.getRemaining();
    } else {
      return store.getAll();
    }
  }

  _update() {
    this.innerHTML = `
      <section>
        <todomvc-header></todomvc-header>
        ${this._todos.length ? `
          <section>
            <input id="toggle-all" class="toggle-all" type="checkbox" ${store.areAllCompleted() ? "checked" : ''}>
            <label for="toggle-all">Mark all as complete</label>

            <ul>
              ${this._todos.map((todo, index) => `
                <todomvc-item id="${todo.id}" text="${todo.text}" ${todo.completed ? 'completed=""' : ''}></todomvc-item>
              `).join('')}
            </ul>
          </section>
        ` : ''}
        
      <style>${styles}</style>
    `;

    this.querySelector('todomvc-header').addEventListener('todomvc-item-added', this._handleTodoItemAdded.bind(this));

    if (this._todos.length) {
      this.querySelector('.toggle-all').addEventListener('click', this._handleToggleAllClick.bind(this));

      this.querySelectorAll('todomvc-item')
        .forEach((todoItemElement) => {
          todoItemElement.addEventListener('todomvc-item-removed', this._handleTodoItemRemoved.bind(this));
          todoItemElement.addEventListener('todomvc-item-completed-changed', this._handleTodoItemCompletedChanged.bind(this));
        });
    }
  }

  _handleToggleAllClick(event) {
    store.setAllTo(!store.areAllCompleted());
    this._update();
  }

  _handleTodoItemAdded(event) {
    store.addTodoWithText(event.detail);
    this._update();
  }

  _handleTodoItemRemoved(event) {
    store.removeById(event.detail);
    this._update();
  }

  _handleTodoItemCompletedChanged(event) {
    store.updateCompleted(event.detail.id, event.detail.completed);
    this._update();
  }

}


if (!customElements.get('todomvc-list')) {
  customElements.define('todomvc-list', TodoMVCList);
}
