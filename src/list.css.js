export default `
todomvc-list {
  display: block;
}

todomvc-list > section {
  background: #fff;
  margin: 130px 0 40px 0;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2),
    0 25px 50px 0 rgba(0, 0, 0, 0.1);
}

todomvc-list ul {
  margin: 0;
  padding: 0;
  list-style: none;
}

todomvc-list > section section {
  position: relative;
}

todomvc-list > section section .toggle-all {
  width: 1px;
  height: 1px;
  border: none; /* Mobile Safari */
  opacity: 0;
  position: absolute;
  right: 100%;
  bottom: 100%;
}

todomvc-list > section section .toggle-all + label {
  width: 60px;
  height: 34px;
  font-size: 0;
  position: absolute;
  top: -52px;
  left: -13px;
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg);
}

todomvc-list > section section .toggle-all + label:before {
  content: '❯';
  font-size: 22px;
  color: #e6e6e6;
  padding: 10px 27px 10px 27px;
}

todomvc-list > section section .toggle-all:checked + label:before {
  color: #737373;
}
`;
