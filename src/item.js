import styles from './item.css.js';

const template = document.createElement('template');
template.setAttribute("id", "itemTemplate");
template.innerHTML = `
  <li>
    <div class="view">
      <input class="toggle" type="checkbox">
      <label></label>
      <button class="destroy"></button>
    </div>
  </li>

  <style>${styles}</style>
`;
document.querySelector("body").appendChild(template);

export class TodoMVCItem extends HTMLElement {
  _id;
  _text;
  _completed = false;

  static get observedAttributes() { return ['id', 'text', 'completed']; }

  constructor() {
    super();
    const itemTemplate = document.getElementById("itemTemplate");
    this.appendChild(itemTemplate.content.cloneNode(true));
  }

  get id() {
    return this._id;

    // .. or when "not" using a separate `_id` property:
    // return parseInt(this.getAttribute('id'));
  }

  set id(newValue) {
    this.setAttribute('id', newValue);
  }

  get text() {
    return this._text;

    // .. or when "not" using a separate `_text` property:
    // return this.getAttribute('text');
  }

  set text(newValue) {
    this.setAttribute('text', newValue);
  }

  get completed() {
    return this._completed;

    // .. or when "not" using a separate `_completed` property:
    // return this.hasAttribute('completed');
  }

  set completed(newValue) {
    if (newValue) {
      this.setAttribute('completed', '');
    } else {
      this.removeAttribute('completed');
    }
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'id') {
      this._id = parseInt(newValue);
    } else if (name === 'text') {
      this._text = newValue;
    } else if (name === 'completed') {
      this._completed = this.hasAttribute('completed');
    }

    if (this._id && this._text && this._completed !== undefined) {
      this._update();
    }
  }

  _update() {
    this.querySelector('li').classList.toggle('completed', this.completed === true);
    if (this.completed) {
      this.querySelector('input').setAttribute("checked", "checked");
    }
    this.querySelector('label').innerHTML = this.text;

    this.querySelector('input')
      .addEventListener('click', this._handleToggleClick.bind(this));
    this.querySelector('button.destroy')
      .addEventListener('click', this._handleDestroyButtonClick.bind(this));
  }

  _handleToggleClick(event) {
    this.dispatchEvent(new CustomEvent('todomvc-item-completed-changed', {detail: {id: this.id, completed: event.target.checked}}));
  }

  _handleDestroyButtonClick(event) {
    this.dispatchEvent(new CustomEvent('todomvc-item-removed', {detail: this.id}));
  }
}


if (!customElements.get('todomvc-item')) {
  customElements.define('todomvc-item', TodoMVCItem);
}
