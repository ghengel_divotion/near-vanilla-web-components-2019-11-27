import './list.js';
import styles from './app.css.js';

class TodoMVCApp extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
      <todomvc-list></todomvc-list>

      <style>${styles}</style>
    `;
  }
}

if (!customElements.get('todomvc-app')) {
  customElements.define('todomvc-app', TodoMVCApp);
}
