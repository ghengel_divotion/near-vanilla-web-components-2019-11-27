export default `
todomvc-header {
  display: block;
}

todomvc-header input:focus {
  outline: 0;
}

todomvc-header h1 {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  font-size: 80px;
  font-weight: 200;
  text-align: center;
  color: #b83f45;
}

todomvc-header input {
  border: 1px solid #999;
  width: 100%;
  font-size: 24px;
  line-height: 1.4em;
  box-shadow: inset 0 -1px 5px 0 rgba(0, 0, 0, 0.2);
  box-sizing: border-box;
}

todomvc-header input {
  box-shadow: inset 0 -2px 1px rgba(0,0,0,0.03);
  border: none;
  padding: 16px 16px 16px 60px;
  background: rgba(0, 0, 0, 0.003);
}

todomvc-header input::placeholder {
  font-weight: 300;
  font-style: italic;
  color: rgba(0, 0, 0, 0.4);
}
`;
