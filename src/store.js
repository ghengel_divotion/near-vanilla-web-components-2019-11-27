const localStorageKey = 'todomvc-vanilla-web-components';

const initialTodos = [
  {id: 1, text: 'Pre-filled todo', completed: true}
];

export class TodoMVCStore {
  _todos;
  _lastId = initialTodos.length;

  constructor() {
    const persistedTodosArrayAsJson = localStorage.getItem(localStorageKey);

    this._todos = persistedTodosArrayAsJson ? JSON.parse(persistedTodosArrayAsJson) : initialTodos;
  }

  areAllCompleted() {
    return this._todos.length === this.getCompleted().length;
  }

  setAllTo(completed) {
    this._todos.forEach((todo) => todo.completed = completed);
    this._persist();
  }

  removeCompleted() {
    this._todos = this._get({ completed: false });
    this._persist();
  }

  getAll() {
    return this._todos;
  }

  getRemaining() {
    return this._get({ completed: false });
  }

  getCompleted() {
    return this._get({ completed: true });
  }

  toggleCompletionById(id) {
    let todo = this._findById(id);

    if (todo) {
      todo.completed = !todo.completed;
      this._persist();
    }
  }

  removeById(id) {
    let index = this._findIndex(id);

    if (index !== -1) {
      this._todos.splice(index, 1);
      this._persist();
    }
  }

  addTodoWithText(text) {
    const id = this._lastId + 1;

    this._todos.push({id, text, completed: false});
    this._persist();

    this._lastId = id;
  }

  updateCompleted(id, updatedCompleted) {
    const index = this._findIndex(id);

    if (index !== -1) {
      const existingTodo = this._todos[index];

      this._todos[index] = {...existingTodo, completed: updatedCompleted};
    }

    this._persist();
  }

  _persist() {
    localStorage.setItem(localStorageKey, JSON.stringify(this._todos));
  }

  _get(state) {
    return this._todos.filter((todo) => todo.completed === state.completed);
  }

  _findById(id) {
    return this._todos.find((todo) => todo.id === id);
  }

  _findIndex(id) {
    return this._todos.findIndex((todo) => todo.id === id);
  }
}

export const store = new TodoMVCStore();
